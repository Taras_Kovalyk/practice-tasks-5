using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace PracticeTasks.Task2
{
    /// <summary>
    /// Class is a generic implementation of polynomial.
    /// It contains a list of items with value and degree.
    /// There are a few overloaded operations. 
    /// 
    /// Besided type parameter can only be non-nullable,
    /// there are types that can be used here for presentation
    /// but not for arithmetic operations and it causes 
    /// exception (DateTime for example), be careful.
    /// It works fine for any numeric type anyway.
    /// 
    /// Although class looks a bit over engineered, it's
    /// a price paid for being generic and most importantly,
    /// saving a lot of memory for sparse polynomial.
    /// </summary>
    /// <typeparam name="T">Type parameter should be non-nullable.</typeparam>
    public class Polynomial<T>
            where T : struct
    {
        #region Constructors
        public Polynomial()
        {
            Coefficients = new List<PolynomialItem>();
        }

        /// <summary>
        /// Creates an instance with specified coefficients,
        /// starting with zero degree.
        /// </summary>
        /// <param name="coefficients">Count of them is a degree of polynomial. 
        /// Your argument should contain zero coefficient if they are in polynomial.</param>
        public Polynomial(IEnumerable<T> coefficients)
        {
            Coefficients = coefficients.Select((coef, deg) => new PolynomialItem(coef, deg))
                                       .Where((item) => !item.Value.Equals(default(T)))
                                       .ToList();
        }

        /// <summary>
        /// Mostly for developers use, but users can instantiate with this way too.
        /// </summary>
        public Polynomial(List<PolynomialItem> coefficients)
        {
            Coefficients = coefficients;
        }

        /// <summary>
        /// If you need sparse polynomial, it's the most appropriate constructor.
        /// </summary>
        /// <param name="coefficients"></param>
        public Polynomial(params PolynomialItem[] coefficients)
        {
            var degrees = coefficients.Select(item => item.Degree).ToList();
            if (degrees.Count() != degrees.Distinct().Count())
            {
                throw new ArgumentException("Entries with the same degree.");
            }

            var coefficientsWithCorrectOrder = coefficients.OrderBy(item => item.Degree);
            Coefficients = coefficientsWithCorrectOrder.ToList();
        }
        #endregion

        #region Properties
        public List<PolynomialItem> Coefficients { get; private set; }

        public int Degree
        {
            get
            {
                return Coefficients.Last().Degree;
            }
        }
        #endregion

        #region Overloaded operators
        public static Polynomial<T> operator +(Polynomial<T> me, Polynomial<T> other)
        {
            var newCoefficients = ProcessRepetitiveEntries(me.Coefficients.Union(other.Coefficients),
                                                           (first, second) => (dynamic)first + (dynamic)second)
                                  .Where(item => !item.Value.Equals(default(T)))
                                  .OrderBy(item => item.Degree)
                                  .ToList();
            return new Polynomial<T>(newCoefficients);
        }

        public static Polynomial<T> operator -(Polynomial<T> me, Polynomial<T> other)
        {
            var res = me + new Polynomial<T>(other.Coefficients
                        .Select(item => new PolynomialItem(((dynamic)item.Value * (-1)), item.Degree))
                        .ToList());

            return res;
        }

        public static Polynomial<T> operator *(Polynomial<T> me, Polynomial<T> other)
        {
            var Product = (from mine in me.Coefficients
                           from others in other.Coefficients
                           select
                               (ProcessRepetitiveEntries(
                                   new[]{ new PolynomialItem(mine.Value, mine.Degree + others.Degree),
                                           new PolynomialItem(others.Value, mine.Degree + others.Degree) },
                                           (first, second) => first.Equals(default(T)) ?
                                                                    (dynamic)second : (dynamic)first * (dynamic)second)
                               ).FirstOrDefault())
                           .ToList();

            var newCoefficients =
                        ProcessRepetitiveEntries(Product, (first, second) => (dynamic)first + (dynamic)second)
                        .Where(item => !item.Value.Equals(default(T)))
                        .OrderBy(item => item.Degree)
                        .ToList();
            return new Polynomial<T>(newCoefficients);
        }
        #endregion

        #region Methods
        public T DoSubstitution(T variable)
        {
            return DoCalculate<PolynomialItem>(this.Coefficients, (first, second) =>
            {
                return (dynamic)first + ((dynamic)second.Value * (dynamic)(T)Math.Pow((dynamic)variable, second.Degree));
            });
        }

        /// <summary>
        /// Special method to make arithmetic operations more safe
        /// and controllable. It's necessary because of generic kind of class.
        /// It helps to avoid WET too.
        /// </summary>
        private static T DoCalculate<T2>(IEnumerable<T2> collection, Func<T, T2, T> func)
        {
            try
            {
                dynamic res = default(T);

                foreach (var item in collection)
                {
                    res = func(res, item);
                }

                return res;
            }
            catch
            {
                throw new InvalidOperationException(
                    String.Format("Arithmetic operations aren't allowed for type parameter {0}", typeof(T)));
            }
        }

        /// <summary>
        /// We can get rid of elements with the same degree
        /// with specified func.
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="func"></param>
        /// <returns></returns>
        private static IEnumerable<PolynomialItem> ProcessRepetitiveEntries(IEnumerable<PolynomialItem> collection,
                                                                            Func<T, T, T> func)
        {
            return from item in collection
                   group item by item.Degree into gr
                   let deg = gr.Key
                   let val = DoCalculate<T>(gr.Select(item => item.Value), func)
                   select new PolynomialItem(val, deg);
        }

        public override string ToString()
        {
            StringBuilder name = new StringBuilder();
            foreach (var item in Coefficients)
            {
                string template = String.Empty;
                switch (item.Degree)
                {
                    case 0:
                        template = "{0}";
                        break;
                    case 1:
                        template = "{0}*x";
                        break;
                    default:
                        template = "{0}*x^{1}";
                        break;
                }

                name.Append(String.Format(template, item.Value, item.Degree));

                if (item.Degree != this.Degree)
                {
                    name.Append(" + ");
                }
            }

            return name.ToString();
        }
        #endregion

        #region PolynomialItem class
        public class PolynomialItem
        {
            private int degree;

            public PolynomialItem() { }

            public PolynomialItem(T num, int degree)
            {
                this.Value = num;
                this.Degree = degree;
            }

            public T Value { get; private set; }

            public int Degree
            {
                get
                {
                    return degree;
                }
                private set
                {
                    if (value < 0)
                    {
                        throw new ArgumentException("Degree is less than 0.");
                    }

                    degree = value;
                }
            }
        }
        #endregion
    }
}