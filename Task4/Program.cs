using System;

namespace PracticeTasks.Task4
{
    public class Program
    {
        public static void Main()
        {
            // Instantiation
            Rectangle rectangle1 = new Rectangle(0, 5, 5, 5);
            Rectangle rectangle2 = new Rectangle(2, 2, 1, 1);
            
            Console.WriteLine("==== Rectangles info");
            Console.WriteLine(rectangle1);
            Console.WriteLine(rectangle2);
            Console.WriteLine();

            Console.WriteLine("==== Moving first rectangle 3 point right, ");
            Console.WriteLine("==== 1 point left, 1 point up and 5 point down.");
            rectangle1.Move(Direction.Right, 3);
            rectangle1.Move(Direction.Left, 1);
            rectangle1.Move(Direction.Up, 1);
            rectangle1.Move(Direction.Down, 5);
            Console.WriteLine(rectangle1);
            Console.WriteLine();

            Console.WriteLine("==== Changing the size of the second rectangle.");
            rectangle2.Height = 4;
            rectangle2.Width = 2;
            Console.WriteLine(rectangle2);
            Console.WriteLine();

            Console.WriteLine("==== Finding the smallest rectangle that contains two others provided.");
            Console.WriteLine("==== So, we have this 2 rectangles:\n{0} \n{1}", rectangle1, rectangle2);
            Console.WriteLine("==== And it's their union:\n{0}", Rectangle.Unite(rectangle1, rectangle2));
            Console.WriteLine();
            
            Console.WriteLine("==== And intersection of course:\n{0}", Rectangle.Intersect(rectangle1, rectangle2));
        }
    }
}