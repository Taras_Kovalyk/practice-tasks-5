using System;
using System.Collections;

namespace PracticeTasks.Task3
{
    public class Program
    {
        public static void Main()
        {
            // Instantiation
            var ternaryVector1 = new TernaryVector(1, 0, 2);
            var ternaryVector2 = new TernaryVector(new int[] { 0, 2, 1 });
            var ternaryVector3 = new TernaryVector(1, 2, 2, 2, 2, 2);
            
            Console.WriteLine("==== Simple vectors");
            PrintVector(ternaryVector1);
            PrintVector(ternaryVector2);
            PrintVector(ternaryVector3);
            Console.WriteLine();

            // demo
            Console.WriteLine("==== You can check if two vector are orthogonal. First two for example are{0}.",
                              TernaryVector.AreOrthogonal(ternaryVector1, ternaryVector2) ? "" : " not");
            Console.WriteLine();

            Console.WriteLine("==== Logical intersection of vectors provided.");
            Console.WriteLine("==== (Vectors with different length cause InvalidOperationException.");
            PrintVector(ternaryVector1 + ternaryVector2);
            Console.WriteLine();

            Console.WriteLine("==== You can check how many items with specified value are presented.");
            Console.WriteLine("==== There are {0} twos in the third vector.", ternaryVector3.CountOf(2));
            Console.WriteLine();
        }

        public static void PrintVector(IEnumerable vector)
        {
            foreach (var item in vector)
            {
                Console.Write(item);
                Console.Write("   ");
            }
            Console.WriteLine();
        }
    }
}