using System;

namespace PracticeTasks.Task4
{
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }

    public class Point
    {
        public float X { get; set; }
        public float Y { get; set; }

        public Point() { }
        public Point(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }
    }

    /// <summary>
    /// Implementation of rectangle parallel axes. 
    /// Class allows to move rectangle, change size.
    /// Union and intersection of two rectangles can easily be found using static methods.
    /// 
    /// Class operates with top left point of rectangle and its sizes.
    /// </summary>
    public class Rectangle
    {
        #region Constructors
        public Rectangle()
        {
            TopLeftPoint = new Point();
        }

        public Rectangle(float topLeftX, float topLeftY, float height, float width)
        {
            this.TopLeftPoint = new Point(topLeftX, topLeftY);
            this.Height = height;
            this.Width = width;
        }
        #endregion

        #region Properties
        public Point TopLeftPoint { get; private set; }
        public float Height { get; set; }
        public float Width { get; set; }
        #endregion

        #region Methods
        public void Move(Direction direction, float distance)
        {
            switch (direction)
            {
                case Direction.Up:
                    TopLeftPoint.Y += distance;
                    break;
                case Direction.Down:
                    TopLeftPoint.Y -= distance;
                    break;
                case Direction.Left:
                    TopLeftPoint.X -= distance;
                    break;
                case Direction.Right:
                    TopLeftPoint.X += distance;
                    break;
                default: break;
            }
        }

        public override string ToString()
        {
            return String.Format("Top left point: [{0},{1}]; Height: {2}; Width: {3};",
                                            TopLeftPoint.X,
                                            TopLeftPoint.Y,
                                            Height,
                                            Width);
        }

        #region Static Methods

        public static Rectangle Unite(Rectangle first, Rectangle second)
        {
            Rectangle res = new Rectangle();

            res.TopLeftPoint.X = Math.Min(first.TopLeftPoint.X, second.TopLeftPoint.X);
            res.TopLeftPoint.Y = Math.Max(first.TopLeftPoint.Y, second.TopLeftPoint.Y);

            Point lowerRightPoint = FindTheLowestRightPoint(first, second);

            res.Height = res.TopLeftPoint.Y - lowerRightPoint.Y;
            res.Width = lowerRightPoint.X - res.TopLeftPoint.X;

            return res;
        }

        public static Rectangle Intersect(Rectangle first, Rectangle second)
        {
            Rectangle res = new Rectangle();

            if (IsIntersection(first, second))
            {
                res.TopLeftPoint.Y = Math.Min(first.TopLeftPoint.Y, second.TopLeftPoint.Y);
                res.TopLeftPoint.X = Math.Max(first.TopLeftPoint.X, second.TopLeftPoint.X);
                res.Height = res.TopLeftPoint.Y -
                                Math.Max(first.TopLeftPoint.Y - first.Height, second.TopLeftPoint.Y - second.Height);
                res.Width = Math.Min(first.TopLeftPoint.X + first.Width, second.TopLeftPoint.X + second.Width) -
                                res.TopLeftPoint.X;
            }

            return res;
        }

        /// <summary>
        /// Finding the lowest right point of all points that two rectangles contains.
        /// </summary>
        private static Point FindTheLowestRightPoint(Rectangle first, Rectangle second)
        {
            Point lowerRightPoint = new Point();

            lowerRightPoint.X = Math.Max(first.TopLeftPoint.X + first.Width, second.TopLeftPoint.X + second.Width);
            lowerRightPoint.Y = Math.Min(first.TopLeftPoint.Y - first.Height, second.TopLeftPoint.Y - second.Height);

            return lowerRightPoint;
        }

        /// <summary>
        /// Checking if two rectangle have non-empty intersection at all.
        /// </summary>
        private static bool IsIntersection(Rectangle first, Rectangle second)
        {
            return (first.TopLeftPoint.Y - first.Height > second.TopLeftPoint.Y ||
                    first.TopLeftPoint.Y < second.TopLeftPoint.Y - second.Height ||
                    first.TopLeftPoint.X > second.TopLeftPoint.X + second.Width ||
                    first.TopLeftPoint.X + first.Width < second.TopLeftPoint.X) ? false : true;
        }
        #endregion
        #endregion
    }
}